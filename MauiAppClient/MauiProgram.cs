﻿using MauiAppClient.ViewModel;

namespace MauiAppClient
{
    public static class MauiProgram
    {
        public static MauiApp CreateMauiApp()
        {
            var builder = MauiApp.CreateBuilder();
            builder
                .UseMauiApp<App>()
                .ConfigureFonts(fonts =>
                {
                    fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
                    fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");

                    fonts.AddFont("DS-DIGI-1.ttf", "DS-DIGI-1");
                    fonts.AddFont("DS-DIGIB-2.ttf", "DS-DIGI-2");
                    fonts.AddFont("DS-DIGII-3.ttf", "DS-DIGI-3");
                    fonts.AddFont("DS-DIGIT-4.ttf", "DS-DIGI-4");
                });
            builder.Services.AddSingleton<MainPage>();
            builder.Services.AddSingleton<MainPageModel>();
            return builder.Build();
        }
    }
}