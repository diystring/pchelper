﻿using CommunityToolkit.Mvvm.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MauiAppClient.ViewModel
{
    public partial class MainPageModel : ObservableObject
    {
        [ObservableProperty]
        public string strTime;

        [ObservableProperty]
        public string strTips;

        [ObservableProperty]
        public string strKey;

        [ObservableProperty]
        public string strValue1;

        [ObservableProperty]
        public string strValue2;
    }
}
