﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MauiAppClient.ViewModel
{
    public class MsgModel
    {
        public string query { get; set; }
        public List<string> translation { get; set; }
        public string errorCode { get; set; }
        public Basic basic { get; set; }
    }

    public class Basic
    {
        public List<string> explains { get; set; }
    }
}
